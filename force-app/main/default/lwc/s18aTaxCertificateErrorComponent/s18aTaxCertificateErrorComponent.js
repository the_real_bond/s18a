import { LightningElement, api } from 'lwc';

export default class S18aTaxCertificateErrorComponent extends LightningElement {
    @api errorMessage
    @api error

    get errorString() {
        return JSON.stringify(this.error);
    }
}