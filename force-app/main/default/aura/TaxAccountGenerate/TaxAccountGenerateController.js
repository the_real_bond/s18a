({
    doInit : function(component, event, helper) {
		//helper.checkTaxRecords(component);  
        helper.checkAccess(component);
        helper.fetchSelectOptions(component);  
        
        console.log("Con ended")
	},  
    updateSelectedOption : function(component, event, helper) {
        component.set('v.taxYear', component.find("typeSelector").get("v.value"));
    },  
  	createTaxCert : function(component, event, helper) {
      var action = component.get("c.createTaxCertificate");
        console.log(component.get("v.taxYear") );
      	action.setParams({
                "accountId" : component.get("v.recordId"),
            	"taxYear" : component.get("v.taxYear") 
        });
      	// set call back 
        action.setCallback(this, function(response) {
          let state = response.getState();
          if (state === "SUCCESS") {
            var result = response.getReturnValue();
            console.log(result);
            var sObectEvent = $A.get("e.force:navigateToSObject");
            sObectEvent .setParams({
              "recordId": result 
            });
              sObectEvent.fire();  
          }
          else if (state === "ERROR") {
              let errors = response.getError();
              let message = 'Unknown error'; // Default error message
              // Retrieve the error message sent by the server
              if (errors && Array.isArray(errors) && errors.length > 0) {
                  message = errors[0].message;
              }
              helper.handleErrors(errors);
              console.error(message);
          }
          else {
              // Handle other reponse states
          }
               
        });
        // enqueue the action
        $A.enqueueAction(action);
   }   
})