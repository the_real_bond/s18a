/**
 * @description       : 
 * @author            : Lian Bond
 * @group             : 
 * @last modified on  : 10-12-2020
 * @last modified by  : Lian Bond
 * Modifications Log 
 * Ver   Date         Author      Modification
 * 1.0   08-12-2020   Lian Bond   Initial VersionS18ADomainLayer
**/
public with sharing class TaxCertificateCreate {
    
    public static List<Tax_Certificate_Request__c> createTaxCertificates(List<Tax_Certificate_Request__c> tcrList) {
            
            system.debug('TaxCertificateCreate.createTaxCertificates new tax certificate requests : ' + tcrList);
            date startDate;
            date endDate;
            Date today = system.today();
            Integer currentFY;
            Tax_Document_Configuration__mdt tdc;
            
            Set<Id>  accountIds = new Set<Id>();
            Set<Id>  taxCertIds = new Set<Id>();
            List<Id>  existingTaxCertIds = new List<Id>();//existingTaxCerts that need to be regenerated
            
            List<Tax_Certificate__c> newTaxCerts = new List<Tax_Certificate__c>();
            List<npe01__OppPayment__c> paymentList;
            List<Tax_Document_Detail__c> taxJunctionList = new List<Tax_Document_Detail__c>(); 
            List<ContentVersion> newContentVersions = new List<ContentVersion>();
            List<Log__c> errorLogs = new List<Log__c>();
            
            Map<Id,Account> accounts = new Map<Id,Account>();
            Map<Id,Tax_Certificate__c> AccountTaxCerts = new Map<Id,Tax_Certificate__c>();
            Map<Id,List<npe01__OppPayment__c>> AccountOppPayments = new Map<Id,List<npe01__OppPayment__c>>();
            Map<Id,decimal> AccountTotalMap = new Map<Id,decimal>();
            
            tdc = 
                [
                    SELECT
                    Retention_Period_For_Logs_Years__c,
                    Retention_Period_For_Tax_Certs_Years__c,
                    Special_Donor_Amount__c,
                    Tax_Document_Footer_Image_URL__c,
                    Tax_Document_Logo_Image_URL__c,
                    Tax_Document_Minimum_Amount__c,
                    Tax_Document_Number_Prefix__c,
                    Tax_Year_End_Day__c,
                    Tax_Year_End_Month__c,
                    Tax_Year_Start_Day__c,
                    Tax_Year_Start_Month__c,
                    Tax_certificates_available_from__c
                    FROM
                    Tax_Document_Configuration__mdt
                ];
            
            Integer newTaxCertsCount = 0;
            Integer existingTaxCerts = 0;
            
            for (Tax_Certificate_Request__c tcr : tcrList) { 
                accountIds.add(tcr.Account__r.Id);
                
                // if(tcr.taxCertId != null) {
                //         existingTaxCertIds.add(tcr.taxCertId);
                // }
                
                // if(tcr.taxCertId == null) {
                //     newTaxCertsCount++;
                // }
                // else {
                //         existingTaxCerts++;
                // }                
            }
            
            // if((newTaxCertsCount > 0 && existingTaxCerts > 0) || existingTaxCerts > 1)//Cannot regenerate and create new tax certs at the same time and 
            // {																	   //cannot regenerate more than 1 tax cert at a time due to the way the dates are handled below.
            //     errorLogs.add (
            //         new ApexDebugLog().createLogNoDML (
            //             new ApexDebugLog.Error (
            //                 'TaxCertificateCreate',
            //                 'createTaxCertificates',
            //                 String.valueOf(tcrList[0].getAccountId()),
            //                 tcrList[0].batch,
            //                 new NoDonationsException('Cannot regenerate and create new tax certs at the same time and cannot regenerate more than 1 tax cert at a time due to the dates are handled')
            //             )
            //         )
            //     );
            //     insert errorLogs;
            //     return tcrList;
            // }

            // if(existingTaxCertIds.size() > 0)//if a tax a cert is being regenerated then delete   							
            // { 								 //existing details and PDF's as they will be replaced.
            //     List<Tax_Document_Detail__c> taxDocDetailsDelete = 
            //         [
            //             SELECT
            //             Id
            //             FROM
            //             Tax_Document_Detail__c
            //             WHERE 
            //             Tax_Certificate__c in :existingTaxCertIds
            //         ];
            //     delete taxDocDetailsDelete;
                
            //     List<ContentDocumentLink> contentDocumentsLinks = 
            //         [
            //             SELECT
            //             Id,
            //             ContentDocumentId
            //             FROM
            //             ContentDocumentLink
            //             WHERE 
            //             LinkedEntityId in :existingTaxCertIds
            //         ];
                
            //     Set<Id> contentDocIds =  new Set<Id>();
                
            //     for (ContentDocumentLink CDL : contentDocumentsLinks) {   
            //         contentDocIds.add(CDL.ContentDocumentId);
            //     }
                
            //     List<contentDocument> contentDocumentsDelete = 
            //         [
            //             SELECT
            //             Id
            //             FROM
            //             contentDocument
            //             WHERE 
            //             Id in :contentDocIds
            //         ];
                                
            //     delete contentDocumentsDelete;
            // }
            
            accounts = new Map<Id,Account>
                ([
                    SELECT
                    Id,
                    name,
                    npe01__One2OneContact__c,
                    BillingStreet,
                    BillingCity,
                    BillingState,
                    BillingCountry
                    FROM
                    Account
                    WHERE
                    Id = : accountIds
                ]);
            
            if (today.month() >= Integer.valueOf(tdc.Tax_Year_Start_Month__c))  {
                currentFY = today.year();
            } 
            else {
                currentFY = today.year() - 1;
            }
            
            if(tcrList[0].From_Date__c != null) {
                startDate = tcrList[0].From_Date__c;
                endDate = tcrList[0].To_Date__c;
            }
            else {
                startDate = date.newInstance(currentFY, Integer.valueOf(tdc.Tax_Year_Start_Month__c), 01);
                endDate = date.newInstance(currentFY+ 1, Integer.valueOf(tdc.Tax_Year_Start_Month__c) - 1, Date.daysInMonth(currentFY + 1, Integer.valueOf(tdc.Tax_Year_Start_Month__c) - 1)); 
            }  

            paymentList =
                [
                    SELECT
                    Id,
                    npe01__Payment_Amount__c,
                    Tax_Certificate_Generated__c,
                    npe01__Opportunity__r.AccountId
                    FROM
                    npe01__OppPayment__c
                    WHERE
                    npe01__Opportunity__r.AccountId in : accountIds
                    AND
                    npe01__Payment_Date__c >= : startDate
                    AND
                    npe01__Payment_Date__c <= : endDate
                    AND
                    npe01__PaId__c = true
                    AND
                    npe01__Payment_Amount__c != null
                    AND
                    npe01__Payment_Amount__c > 0
                    AND 
                    Tax_Certificate_Generated__c = false
                ];
            
            
            
            for(npe01__OppPayment__c payRec : paymentList) {
                
                Decimal totalDonationAmount = AccountTotalMap.get(payRec.npe01__Opportunity__r.AccountId);
                //List<npe01__OppPayment__c> tempList = AccountOppPayments.get(payRec.npe01__Opportunity__r.AccountId);
                
                if(totalDonationAmount == null) {
                    totalDonationAmount = 0;
                }
                
                // if(tempList == null) {
                //     tempList =  new List<npe01__OppPayment__c>();
                // }
                
                //tempList.add(payRec);
                totalDonationAmount += payRec.npe01__Payment_Amount__c;
                AccountTotalMap.put(payRec.npe01__Opportunity__r.AccountId, totalDonationAmount);
                
                //AccountOppPayments.put(payRec.npe01__Opportunity__r.AccountId,tempList);
            }


            
            for (Tax_Certificate_Request__c tcr : tcrList) {
                tcr.Total_Donations__c = AccountTotalMap.get(tcr.Account__c);
                tcr.Status__c = 'Delivered';
                if (tcr.Total_Donations__c!= null && tcr.Total_Donations__c >= 100) {
                    
                    Account acnt = accounts.get(tcr.Account__c);
                    
                    Tax_Certificate__c tc = new Tax_Certificate__c();
                    
                    //tc.Id = tcr.taxCertId;
                    tc.Account__c = tcr.Account__r.Id;
                    tc.Contact__c = acnt.npe01__One2OneContact__c;
                    tc.Generated__c = false;
                    tc.Sent__c = false;
                    tc.Tax_Certificate_Request__c = tcr.Id;
                    
                    if (!Test.isRunningTest()) {
                        tc.Special_Donor__c = tcr.Total_Donations__c >= tdc.Special_Donor_Amount__c ? true : false;
                    } else {
                        tc.Special_Donor__c = true;
                    }  
                    
                    tc.Tax_Document_Start_Date__c = startDate;
                    tc.Tax_Document_End_Date__c = endDate;
                    tc.Tax_Certificate_Batch__c = tcr.Tax_Certificate_Batch__c;
                    
                    newTaxCerts.add(tc);
                }
            }
            
            insert newTaxCerts;
            
            for (Tax_Certificate__c taxCert : newTaxCerts) {
                taxCertIds.add(taxCert.Id);
            }
            
            newTaxCerts = 
                [
                    SELECT 
                    Id,
                    Name,
                    Tax_Certificate_Number__c,
                    Account__c
                    FROM 
                    Tax_Certificate__c
                    WHERE 
                    Id in :taxCertIds
                ];
            
            for(Tax_Certificate__c taxCert : newTaxCerts ) {
                if (!Test.isRunningTest()) {
                    taxCert.Tax_Certificate_Number__c = tdc.Tax_Document_Number_Prefix__c + taxCert.Name + startDate.year();
                } else {
                    taxCert.Tax_Certificate_Number__c = 'TestCert123';
                }    
                AccountTaxCerts.put(taxCert.Account__c, taxCert);
            }
            
            update newTaxCerts; 
            
            for (Tax_Certificate_Request__c tcr : tcrList) {
                if (tcr.Total_Donations__c!= null && tcr.Total_Donations__c >= 100) {
                    Tax_Certificate__c newTaxCert = AccountTaxCerts.get(tcr.Account__c);
                    tcr.Tax_Certificate_Number__c = newTaxCert.Tax_Certificate_Number__c;
                    tcr.Tax_Certificate_Id__c = newTaxCert.Id;
                    
                    //tcr.taxCert = TaxHelperClass.generatePDF(tcr.AccountId, tcr.fromDate, tcr.toDate, tcr.taxCertNumber, tcr.taxCertId);
                    
                    // List<npe01__OppPayment__c> accPaymentList = AccountOppPayments.get(tcr.Account__c);
                    
                    for(npe01__OppPayment__c payRec : paymentList)  { 
                        if(tcr.Account__c == payRec.npe01__Opportunity__r.AccountId ) {
                            // Tax_Document_Detail__c taxDocDetail = new Tax_Document_Detail__c();
                            // taxDocDetail.Payment__c = payRec.Id;
                            // taxDocDetail.Tax_Certificate__c = tcr.taxCertId; 
                            // taxJunctionList.add(taxDocDetail);
                            payRec.Tax_Certificate__c = newTaxCert.Id;
                            payRec.Tax_Certificate_Generated__c = true;
                        }
                    }
                    
                    /*ContentVersion conVer = new ContentVersion();
                    conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
                    conVer.PathOnClient = tcr.taxCertNumber + '.Pdf'; // The files name, extension is very important here which will help the file in preview.
                    conVer.Title = 'Tax Certificate'; // Display name of the files
                    conVer.VersionData = tcr.taxCert;
                    conVer.FirstPublishLocationId = tcr.taxCertId;
                    newContentVersions.add(conVer);*/
                    
                } else {
                    errorLogs.add (
                        new ApexDebugLog().createLogNoDML (
                            new ApexDebugLog.Error (
                                'TaxCertificateCreate',
                                'createTaxCertificates',
                                String.valueOf(tcr.Account__c),
                                tcr.Tax_Certificate_Batch__c,
                                new NoDonationsException('No donation records found for date range')
                            )
                        )
                    );
                }
            }
            
            
            //insert taxJunctionList;
            //insert newContentVersions;
            
            // for(Tax_Certificate__c taxCert : newTaxCerts ) {
            //     taxCert.Generated__c = true;
            // }   
            
            update paymentList;
            update newTaxCerts;
            insert errorLogs;
            update tcrList;
            return tcrList;
    }     
}