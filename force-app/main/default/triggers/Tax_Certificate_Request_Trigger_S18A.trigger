/**
 * @description       : This trigger controls most processes related to Tax Certificate requests
 * @author            : Lian Bond
 * @group             : 
 * @last modified on  : 25-02-2021
 * @last modified by  : Lian Bond
 * Modifications Log 
 * Ver   Date         Author      Modification
 * 1.0   09-02-2021   Lian Bond   Initial Version
**/
trigger Tax_Certificate_Request_Trigger_S18A on Tax_Certificate_Request__c (before insert, after insert) {
    if (trigger.isInsert) {

        if (trigger.isBefore) {
            for (Tax_Certificate_Request__c tcr : trigger.new) {
                tcr.OTP_Max_Regenerate__c = 3;
                tcr.OTP_Max_Retry__c = 3;
                //TCR.Source__c =  'contactID || accountID || OpportunityID'
                String sourceObjName = ((Id)tcr.Source__c).getSObjectType().getDescribe().getName();
                System.debug('TaxDocumentPDF_Controller: TCR source sobject name ' + sourceObjName);
                tcr.Source_Object_Name__c = sourceObjName;
            }
        }
        
        if (trigger.isAfter) {
            Map<Id, SObject> tcrAndRecepient = new Map<Id, SObject>();
            SObject recipient = null;
            ///reciepient is potentially different depending on the source, clarify with duncan
            for (Tax_Certificate_Request__c tcr : trigger.new) {
                if (tcr.Request_Type__c == 'Account') { ///N
                    recipient = TaxHelperClass.getContactWithAccountId(tcr.Source__c);
                }
                tcrAndRecepient.put(tcr.Id, recipient);
            }
            for (Tax_Certificate_Request__c tcr : trigger.new) {
                
                
                List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>(); 
                //Send email to reciepient of tax cert
                //todo bulkify this email utility
                    
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                List<String> sendTo = new List<String>();
                sendTo.add(tcrAndRecepient.get(tcr.Id).Email);
                mail.setToAddresses(sendTo);
                mail.setReplyTo('noreply@salesforce.com'); // change it with your mail address.
                mail.setSenderDisplayName('Salesforce User'); 
                mail.setSubject('Tax Certificate Request received.');
                mail.setHtmlBody('Please follow this <a href=\'https://s18acs-developer-edition.um6.force.com/taxcert/s/?id=' + tcr.Id + '\'>link ' + '</a> to download your tax certificate');
                mails.add(mail);
                if(test.isRunningTest()== false)
                {
                    Messaging.SendEmailResult[] result = Messaging.sendEmail(mails);
                    if (!result[0].isSuccess()) {
    
                        //LB - todo mark the relevant TCR as contact email invalid
                        throw new AuraHandledException('Contact email invalid ' + result[0].getErrors().get(0).getMessage());
                    }
                }
            }

        }
    }
}