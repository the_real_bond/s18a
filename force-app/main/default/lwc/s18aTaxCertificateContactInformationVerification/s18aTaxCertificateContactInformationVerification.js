import { LightningElement, api, wire, track } from 'lwc';

export default class S18aTaxCertificateContactInformationVerification extends LightningElement {

    @api records;

    handleSuccess(event) {
        //Let container component know that the account contact information verification was successful
        const selectedEvent = new CustomEvent("contactsubmit", {
            
        });
    
        // Dispatches the event.
        this.dispatchEvent(selectedEvent);
        console.log('s18aTaxCertificateContactInformationVerification: contact information verified. Sending evt to container cmp')
    }




}