public class KeyPair {
        
    @AuraEnabled public String theKey { get; set; }
    @AuraEnabled public String theValue { get; set; }
    
    public KeyPair(String inputKey, String inputValue) {
        this.theKey = inputKey;
        this.theValue = inputValue;
    }   
}