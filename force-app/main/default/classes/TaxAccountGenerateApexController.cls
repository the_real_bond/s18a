/**
 * @description       : 
 * @author            : Lian Bond
 * @group             : 
 * @last modified on  : 10-02-2021
 * @last modified by  : Lian Bond
 * Modifications Log 
 * Ver   Date         Author      Modification
 * 1.0   10-12-2020   Lian Bond   Initial Version
**/

//Lian Todo, can you make taxcert exception generalized?
//clean up code
//put mail code below on tax helper
//create a batch class that will automatically check contacts for bounced emails
// /todo bulkify this email utility 
//todo multiple contacts on account
//todo what if there is multiple tax cert requests? what if one already exists on the account
//tax cert request before trigger, check existing
public class TaxAccountGenerateApexController {

    @AuraEnabled 
    public static String createTaxCertificate(Id accountId, Integer taxYear){
        Date fromDate; 
        Date toDate;
        
        try {
            //try to get a contact (recipient related to this account to which an email will be sent)
            Contact recipient = null;
            try {
                recipient = TaxHelperClass.getContactWithAccountId(accountId);
            } catch (Exception e) {
                
            }
            Account acc = TaxHelperClass.getAccount(accountId);
            if (recipient == null ) {
                throw new AuraHandledException('No contact record found for account ' + acc.Name);
            } else if (recipient.Email == null) {
                throw new AuraHandledException('No email found for contact ' + recipient.Name);
            }
            Map<String, Date> taxYearMap = TaxHelperClass.getFullTaxYearDateRange(taxYear);
            
            fromDate = taxYearMap.get('from');
            toDate = taxYearMap.get('to');
            
            List<npe01__OppPayment__c> paymentList = null;
            try {
                System.debug('TaxAccountGenerateApexController.createTaxCertificate checking for payments for account ' + acc.Name + ' in date range from ' + fromDate + ' to ' + toDate);
                paymentList = TaxHelperClass.getPaymentsForAccount(accountId, fromDate, toDate);
            } catch (Exception e) {
                throw e;
            }
            if (paymentList.size()==0) {
                throw new AuraHandledException('No payments found/All payments are already included in tax certificates.');
            }

            Tax_Certificate_Request__c existingActiveTCR = null;
            try {
                System.debug('TaxAccountGenerateApexController.createTaxCertificate checking for payments for account ' + acc.Name + ' in date range from ' + fromDate + ' to ' + toDate);
                existingActiveTCR = TaxHelperClass.getExistingTCRRecordForAccount(accountId);
            } catch (Exception e) {
                
            }
            if (existingActiveTCR != null) {
                if (existingActiveTCR.From_Date__c == fromDate || existingActiveTCR.To_Date__c == toDate) {
                    throw new AuraHandledException('There is already an active Tax Certificate Request with a status of ' + existingActiveTCR.Status__c + ' for the Account ' + acc.Name);
                }
            }
            List<Tax_Certificate_Request__c> tcrList = new List<Tax_Certificate_Request__c>();
            Tax_Certificate_Request__c tcr = new Tax_Certificate_Request__c();
            tcr.Account__c = accountId;
            tcr.Request_Type__c = 'Account';
            tcr.From_Date__c = fromDate;
            tcr.To_Date__c = toDate;
            tcr.Source__c = accountId;
            tcrList.add(tcr);
            insert tcrList;
            
            //TaxCertificateCreate.createTaxCertificates(tcrList);
            if(Test.isRunningTest()){
                throw new NoDonationsException();
            }    
        } catch(exception e) {
            new ApexDebugLog().createLog(
                new ApexDebugLog.Error(
                    'axAccountGenerateController',
                    'createTaxCertificate',
                    'accountId',
                    '',
                    e
                )
            );
            if(!Test.isRunningTest()){
                throw e;
            }
        }
        return accountId;
    }
    
    @AuraEnabled 
    public static boolean checkTaxRecordsExist(Id accountId){
        
        Decimal totalDonations = TaxHelperClass.calculateTotalDonations(accountId, null, null);
        System.debug('TOTALDONATIONS:' + totalDonations);
        if (totalDonations > 0) {
            return true;
        }
        return false;    
    }
    
    @AuraEnabled 
    public static boolean haveAccess() {
        
        return TaxHelperClass.userHaveAccess();   
    }
    
    @AuraEnabled
    public static List<KeyPair> getYearsForRun() {
        
        System.debug('Called:  getYearsForRun');
        
        Tax_Document_Configuration__mdt meta = TaxHelperClass.getTaxDocMetadata();
        Date endDate = meta.Tax_certificates_available_from__c;
        Integer endYear = endDate.year();
        Date currentDate = System.today();
        Integer startYear = currentDate.year();
        List<KeyPair> kpList = new List<KeyPair>();
        for (Integer i = startYear; i > endYear; i--) {
            KeyPair kp = new KeyPair(i + '', i + '/' + meta.Tax_Year_Start_Month__c + '/' + meta.Tax_Year_Start_Day__c + ' - ' + (i + 1) + '/' + meta.Tax_Year_End_Month__c + '/' + meta.Tax_Year_End_Day__c);
            kpList.add(kp);
        }
        
        KeyPair kp = new KeyPair(endYear + '', endYear + '/' + meta.Tax_Year_Start_Month__c + '/' + meta.Tax_Year_Start_Day__c + ' - ' + (endYear + 1) + '/' + meta.Tax_Year_End_Month__c + '/' + meta.Tax_Year_End_Day__c);
        kpList.add(kp);
        
        return kpList;
    }
}