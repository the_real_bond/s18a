import { LightningElement, api, track } from 'lwc';
import tcrOtpAuthenticationGenerate from '@salesforce/apex/TaxHelperClass.tcrOtpAuthenticationGenerate';
import Error from '@salesforce/schema/BackgroundOperation.Error';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class S18ATaxCertificateOTP extends LightningElement {
    error;
    otp;
    @track email;
    tcr;
    contact;
    account;
    otpRegenerationCount = 0;

    handleOTPChange(event) {
        this.otp = event.target.value;
    }

    handleSubmitClick() {
         // Creates the event with the data.
        const selectedEvent = new CustomEvent("otpsubmit", {
            detail: this.otp
        });
    
        // Dispatches the event.
        this.dispatchEvent(selectedEvent);
        console.log('event despatched')
    }

    connectedCallback() {
        console.log('s18aAuthentication: Connected... ' + this.records);
        
    }

    set records(value) {
        console.log('S18ATaxCertificateMain.Auth: OTP Sent! 1 ' + JSON.stringify(value));
        if (value) {
            this.tcr = value['tcr'];
            this.contact = value['contact'];
            this.account = value['account'];
            this.email = this.contact.Email; 
            this.otpGenerate();
        }
    }

    otpGenerate() {
        tcrOtpAuthenticationGenerate({tcrId : this.tcr.Id, contactEmail : this.contact.Email})
        .then(result => {
                console.log('S18ATaxCertificateMain.Auth: OTP Sent! ' );
                
        }).catch(error => {
            this.error = error;
            console.error(JSON.stringify(this.error))});
    }

    handleReGen() {
        if (this.otpRegenerationCount <= 3) {
            this.otpGenerate();
            this.showNotification('OTP Authentication', 'success', 'A new OTP has been sent to ' + this.contact.Email);
        } else {
            this.showNotification('OTP Authentication', 'error', 'A new OTP has been sent to ' + this.contact.Email);
        }
        this.otpRegenerationCount++;
    }

    showNotification(title, variant, message) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(evt);
    }

    @api get records() {
        return '';
    }
    
}