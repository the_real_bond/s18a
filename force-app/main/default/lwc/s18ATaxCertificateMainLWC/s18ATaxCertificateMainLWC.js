import { LightningElement, wire, api, track} from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';
import STATUS_FIELD from '@salesforce/schema/Tax_Certificate_Request__c.Status__c';
import getExistingTCRRecord from '@salesforce/apex/TaxHelperClass.getExistingTCRRecord';
import getTCRContact from '@salesforce/apex/TaxHelperClass.getTCRContact';
import tcrOtpAuthenticationValidate from '@salesforce/apex/TaxHelperClass.tcrOtpAuthenticationValidate';
import getTCRAccount from '@salesforce/apex/TaxHelperClass.getTCRAccount';
import { reduceErrors } from 'c/anvil_ldsUtils';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class S18ATaxCertificateMain extends LightningElement {
    
    recordId = '';
    @track tcrRecord; //tax certificate request record
    @track con;
    @track acc = null; 
    @track records;
    @track step1 = true;
    @track step2 = false;
    @track step3 = false;
    parameters = {};
    @track error;

    get reducedErrors() {
        // console.log('ERROR: ' + JSON.stringify(reduceErrors(this.error)));
        return this.error == undefined ? undefined : this.error == null ? undefined : reduceErrors(this.error);
    }
    
    constructor() {
        super();
        //-------------------------->
        //Attaching event listeners
        //<-------------------------
        this.template.addEventListener('otpsubmit', this.otpValidate);
        this.template.addEventListener('contactsubmit', this.otpValidate);
    }

    //------START---------->
    //Attaching event listeners functions
    //<---------------------
    otpValidate = (event) => {
        tcrOtpAuthenticationValidate({tcrId : this.tcrRecord.Id, otp : event.detail})
        .then(result => {
                if(result) {
                    this.showNotification('OTP Authentication','success','OTP Authentication Successful!');
                    this.step1 = false;
                    this.step2 = true;
                    this.step3 = false;
                } else {
                    this.showNotification('OTP Authentication','error','OTP Authentication Failed. Please try again.');
                }
        }).catch(error => {
            this.error = error;
            console.error(JSON.stringify(this.error))});
    }

    contactInfoValidate = () => {
        this.step1 = false;
        this.step2 = false;
        this.step3 = true;
        this.showNotification('Contact Information','success','Contact Information Verified!');
    }

    //--------------------->
    //Attaching event listeners functions
    //<------END------------
    
    connectedCallback() {
        try {
            console.log('S18ATaxCertificateMain.connectedCallback: Connected callback.... ');

            this.recordId = this.getIdFromUrl() ;
            if (this.recordId == undefined) {
                throw new Error('No Tax Certificate Request record ID supplied. Aborting.')
            }
            console.log('S18ATaxCertificateMain: recordId ' + this.recordId);

            this.getRecords();

            
        } catch (e) {
            console.error(e);
            console.error('e.name => ' + e.name );
            console.error('e.message => ' + e.message );
            console.error('e.stack => ' + e.stack );
            this.error = e;
        }
    }
    
    //this method gets the three records needed throughout the process, TCR, Acc and Con
    getRecords() {
        //---------------->
        //get TCR record
        //<----------------
        getExistingTCRRecord({recordId : this.recordId})
        .then(result => {
            console.log('S18ATaxCertificateMain: tax cert req ID ' + this.recordId);
            this.tcrRecord = result;
            //---------------->
            //get account record
            //<----------------
            getTCRAccount({recordId : this.tcrRecord.Account__c})
            .then(result => {
                    this.acc = result;
                    console.log('S18ATaxCertificateMain: acc ' + JSON.stringify(this.acc));
                    //---------------->
                    //get contact record
                    //<----------------
                    getTCRContact({recordId : this.acc.npe01__One2OneContact__c})
                    .then(result => {
                            this.con = result;
                            console.log('S18ATaxCertificateMain: con ' + JSON.stringify(this.con));
                            this.records = {tcr: this.tcrRecord, contact: this.con, account: this.acc};
                    }).catch(error => {
                        this.error = error;
                        console.error(JSON.stringify(this.error))});
            }).catch(error => {
                this.error = error;
                console.error(JSON.stringify(this.error))});
        }).catch(error => {
            this.error = error;
            console.error(JSON.stringify(this.error))});
    }

    getIdFromUrl() {
        
        var id = location.search.substring(1).split('=')[1];
        console.log('S18ATaxCertificateMain.getQueryParameters: Paramaters supplied ' + this.id);
        return id;
    }

    showNotification(title, variant, message) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(evt);
    }

}