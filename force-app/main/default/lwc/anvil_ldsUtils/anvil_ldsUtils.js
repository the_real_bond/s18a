/**
 * PLEASE NOTE: This has been borrowed from https://bitbucket.org/cloudsmiths/the-forge/src/master/anvil/main/default/lwc/ldsUtils/ldsUtils.js ANVIL 1.2 
 *              Thanks for your contribution!
 * 
 * Reduces one or more LDS errors into a string[] of error messages.
 * @param {FetchResponse|FetchResponse[]} errors
 * @return {String[]} Error messages
 */
export function reduceErrors(errors) {
    
    if (!errors) {
        errors = ['Unknown error'];
    }

    if (!Array.isArray(errors)) {
        errors = [errors];
    }

    return (
        errors
            // Remove null/undefined items
            .filter((error) => !!error)
            // Extract an error message
            .map((error) => {
                var errors = [];
                // UI API read errors
                if (Array.isArray(error.body)) {
                    errors = errors.concat(error.body.map((e) => e.message));
                }
                // UI API DML, Apex and network errors
                else if (error.body && typeof error.body.message === 'string') {
                    errors.push(error.body.message);
                }
                // JS errors
                else if (typeof error.message === 'string') {
                    errors.push(error.message);
                }
               // has body?
                if (error.body) {
                    // has Page Errors?
                    if (error.body.pageErrors) {
                        if (Array.isArray(error.body.pageErrors)) {
                            errors = errors.concat(error.body.pageErrors.map((e) => e.message));
                        }
                    }
                    //has Field Errors?
                    if (error.body.fieldErrors) {
                        // these are a map of Field Name : array of error {}
                        Object.keys(error.body.fieldErrors).forEach(key => {
                            console.log(key + ' :' + error.body.fieldErrors[key]) // returns the keys in an object
                            const fieldError = error.body.fieldErrors[key];
                            console.log(fieldError);
                            errors = errors.concat(fieldError.map((e) => e.message));
                        })
                    }
                }
                // Unknown error shape so try HTTP status text
                if (errors.length == 0) {
                    errors.push(error.statusText);
                }
                return errors;
            })
            // Flatten
            .reduce((prev, curr) => prev.concat(curr), [])
            // Remove empty strings
            .filter((message) => !!message)
    );
}