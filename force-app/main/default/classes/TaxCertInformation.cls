/**
 * @description       : 
 * @author            : Lian Bond
 * @group             : 
 * @last modified on  : 08-12-2020
 * @last modified by  : Lian Bond
 * Modifications Log 
 * Ver   Date         Author      Modification
 * 1.0   08-12-2020   Lian Bond   Initial Version
**/
public class TaxCertInformation {
    
    public Id accountId;
    public Id taxCertID;
    public String batch;
    public String taxCertNumber;
    public Date fromDate;
    public Date toDate;   
    public Blob taxCert;
    public Decimal totalDonations; 
    public Map<String, String> certDetails;
    
    public void setAccountId(Id accountId) {
        this.accountId = accountId;
    }
    
    public Id getAccountId() {
        return this.accountId;
    }
    
    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }
    
    public String getBatch() {
        return this.batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }
    
    public Date getFromDate() {
         return fromDate;
    }
    
    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Date getToDate() {
         return toDate;
    }
}