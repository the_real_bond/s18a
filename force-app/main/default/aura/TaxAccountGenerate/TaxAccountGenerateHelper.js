({
    fetchSelectOptions : function(component) {
		
        
        
        //Define action. 
		var action = component.get("c.getYearsForRun"); 
        
        //Callback to handle response.
        action.setCallback(this, function(result) {
            
            var response = result.getReturnValue();
            
            console.log(response);
			component.set("v.taxYearOptions", response);            
            component.set('v.listSize',response.length); 
            component.set('v.taxYear', response[0].theKey);
            
        }); 
        
        //Fire action.
        $A.enqueueAction(action); 
	},
	checkTaxRecords : function(component) {
		
        //Define action.
		var action = component.get("c.checkTaxRecordsExist"); 
        
        //Set method params.
        action.setParams({
            "accountId" : component.get("v.recordId"),
            "taxYear" : component.get("v.taxYear")
        });
        
        //Callback to handle response.
        action.setCallback(this, function(result) {
            var response = result.getReturnValue();
			component.set("v.doesTaxRecordsExist", response);
        }); 
        
        //Fire action.
        $A.enqueueAction(action); 
	},
    checkAccess : function(component) {
		
        //Define action.
		var action = component.get("c.haveAccess"); 
        
        //Callback to handle response.
        action.setCallback(this, function(result) {
            
            var response = result.getReturnValue();
			component.set("v.haveAccess", response);            
        }); 
        
        //Fire action.
        $A.enqueueAction(action); 
    },
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        // Pass the error message if any
        if (errors && Array.isArray(errors) && errors.length > 0) {
            toastParams.message = errors[0].message;
        }
        // Fire error toast
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams(toastParams);
        toastEvent.fire();
    }
})