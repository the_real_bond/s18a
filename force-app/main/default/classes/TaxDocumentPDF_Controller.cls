/**
 * @description       : 
 * @author            : Lian Bond
 * @group             : 
 * @last modified on  : 25-02-2021
 * @last modified by  : Lian Bond
 * Modifications Log 
 * Ver   Date         Author      Modification
 * 1.0   09-02-2021   Lian Bond   Initial Version
**/
public class TaxDocumentPDF_Controller
{
    public Tax_Document_Configuration__mdt Config {get;set;}
    public Tax_Document_Content__c taxDoumentContent {get;set;}
    
    public Contact contactRec {get;set;}
    public Account accountRec {get;set;}
    
    public list<npe01__OppPayment__c> paymentList;
    
    
    public String       myDatetimeStr {get;set;}
    public String       addressStreet {get;set;}
    public String       addressCity {get;set;}
    public String       addressState {get;set;}
    public String       addressPostalCode {get;set;} 
    public String     taxCertificateNumber {get;set;} 
    public String     tcrId {get;set;} 
    
    public decimal totalDonationAmount {get;set;}
    
    public string dateRecieved {get;set;}
    
    public  TaxDocumentPDF_Controller()
    {
        
        //////----------------> get tax certificate record and relevant fields needed for below //get tax cer
        tcrId = Apexpages.currentPage().getParameters().get('taxcertid');
        Tax_Certificate_Request__c tcr =  TaxHelperClass.getExistingTCRRecord(tcrId);
        System.debug('TaxDocumentPDF_Controller: TCR record found');

        
        
        //////----------------> //TCR.Source__c =  'contactID || accountID || OpportunityID' get contact id or account id or opportunity - sourceRecordId
        id accountid = Apexpages.currentPage().getParameters().get('id'); 
        date startDate;
        date endDate;
        totalDonationAmount = 0;
   
        //////----------------> get start from corresponding tax certificate request record fields using id above, get start date from TCR REcord
        // if(Apexpages.currentPage().getParameters().get('startDate') != null)
        // {
        //     startDate = date.valueOf(Apexpages.currentPage().getParameters().get('startDate')); 
        // }
        //  if(Apexpages.currentPage().getParameters().get('endDate') != null)
        // {
        //     endDate =  date.valueOf(Apexpages.currentPage().getParameters().get('endDate')); 
        // }
        


        // if(startDate == null)
        // {
        //     startDate = taxHelperClass.getTaxYearStartDate();
        //     endDate = taxHelperClass.getTaxYearEndDate();
        // }
         
        // string taxCertId = Apexpages.currentPage().getParameters().get('taxcertid'); 
        
        //////---------------->LIAN TODO: get information from account opp or contact using the tax cert id to determine which, DO NOT STORE THIS INFO on TCR, determine at runtime
        taxDoumentContent =  taxHelperClass.getTaxDocumentContent();
        
        // taxCertificateNumber = Apexpages.currentPage().getParameters().get('taxcertno');
        
        if (Test.isRunningTest()) {
            Contact cnt = new Contact();
            cnt.LastName = 'Test Contact';
            insert cnt; 
        
            Account acnt = new Account();
            acnt.npe01__One2OneContact__c = cnt.Id;
            acnt.Name = 'Test Account';
            insert acnt;
            accountid = acnt.Id;
            Map<String, String> certDetails = TaxHelperClass.createTaxCertificateRecord(acnt.Id, false, false, TaxHelperClass.getTaxYearStartDate(), TaxHelperClass.getTaxYearEndDate(), '');
            taxCertificateNumber = certDetails.get('Tax_Certificate_Number__c');
            tcrId = certDetails.get('ID');
        }
        
        
        accountRec = taxHelperClass.getAccount(accountId);
        
        contactRec = taxHelperClass.getContact(accountRec.npe01__One2OneContact__c);
        
       
        
       paymentList =
            [
                SELECT
                id,
                npe01__Payment_Amount__c 
                From
                npe01__OppPayment__c
                Where
                npe01__Opportunity__r.accountid =: accountRec.id //////----------------> example if source__c = contact then get acc id by using relatedRecordId (contact in this case) -> get contact's acco
                AND //use tax year dates
                npe01__Payment_Date__c >= : startDate
                AND
                npe01__Payment_Date__c <= : endDate
                AND
                npe01__Paid__c = true
                AND
                npe01__Payment_Amount__c != null
                AND
                npe01__Payment_Amount__c > 0
            ];
        
        
        dateRecieved = 'Donations from ' +  startDate.format() + ' to '+ enddate.format();
        
        for(npe01__OppPayment__c payRec : paymentList)
        {
            totalDonationAmount += payRec.npe01__Payment_Amount__c;
        }
    
    } 
    
    // public  TaxDocumentPDF_Controller(id accountId, date startDate, date endDate, String taxCertNo , id taxCertId) 
    // {
    //     totalDonationAmount = 0;
        
    //     taxDoumentContent =  taxHelperClass.getTaxDocumentContent();
        
    //     accountRec = taxHelperClass.getAccount(accountId);
        
    //     contactRec = taxHelperClass.getContact(accountRec.npe01__One2OneContact__c);
        
    //     taxCertificateNumber = taxcertno;
    //     tcrId = taxcertid;
       
    //     paymentList =
    //         [
    //             SELECT
    //             id,
    //             npe01__Payment_Amount__c
    //             From
    //             npe01__OppPayment__c
    //             Where
    //             npe01__Opportunity__r.accountid =: accountRec.id
    //             AND
    //             npe01__Payment_Date__c >= : startDate
    //             AND
    //             npe01__Payment_Date__c <= : endDate
    //             AND
    //             npe01__Paid__c = true
    //             AND
    //             npe01__Payment_Amount__c != null
    //             AND
    //             npe01__Payment_Amount__c > 0
    //         ];
        
    //     for(npe01__OppPayment__c payRec : paymentList)
    //     {
    //         totalDonationAmount += payRec.npe01__Payment_Amount__c;
    //     }
       
    // } 
    
    // //Get today's date
    // public static String getTodaysDate() 
    // {
    //     datetime todaysDate  = Datetime.now();
    //     String myDatetimeStr = todaysDate.format('dd/MM/yyyy');
    //     return myDatetimeStr ;
    // }
    
}